<?php

namespace Empu\ElementCrm\Updates;

use Empu\ElementCrm\Models\PipelineStage;
use October\Rain\Database\Updates\Seeder;

class PipelineStagesSeeder extends Seeder
{
    public function run()
    {
        $collection = [
            ['Lead', 0],
            ['Contacted', 0],
            ['Pre-sale', 0],
            ['Closing', 1],
            ['Post-sale', 0],
            ['Upsale', 0],
        ];

        foreach ($collection as $sequence => $value) {
            [$label, $isClosing] = $value;

            PipelineStage::create([
                'label' => $label,
                'sequence' => $sequence,
                'is_closing_stage' => (bool)$isClosing,
            ]);
        }
    }

}