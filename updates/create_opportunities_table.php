<?php

namespace Empu\ElementCrm\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

/**
 * CreateOpportunitiesTable Migration
 */
class CreateOpportunitiesTable extends Migration
{
    public function up()
    {
        Schema::create('empu_elcrm_opportunities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('deal');
            $table->dateTime('deadline')->nullable();
            $table->text('note')->nullable();
            $table->float('probability', 3, 2)->default(0);
            $table->float('amount', 16, 2)->nullable();
            $table->unsignedBigInteger('account_id');
            $table->foreign('account_id')->references('id')->on('empu_crm_parties');
            $table->unsignedInteger('stage_id');
            $table->foreign('stage_id')->references('id')->on('empu_elcrm_pipeline_stages');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('empu_elcrm_opportunities');
    }
}
