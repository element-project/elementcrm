<?php namespace Empu\ElementCrm;

use Backend;
use System\Classes\PluginBase;

/**
 * ElementCrm Plugin Information File
 */
class Plugin extends PluginBase
{
    public $require = [
        'Empu.ElementCore',
    ];

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'ElementCrm',
            'description' => 'No description provided yet...',
            'author'      => 'Empu',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Empu\ElementCrm\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'empu.elementcrm.access_opportunities' => [
                'tab' => 'ElementCrm',
                'label' => 'Access opportunities'
            ],
            'empu.elementcrm.access_tasks' => [
                'tab' => 'ElementCrm',
                'label' => 'Access opportunities'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'elementcrm' => [
                'label'       => 'ElementCrm',
                'url'         => Backend::url('empu/elementcrm/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['empu.elementcrm.*'],
                'order'       => 500,
                'sideMenu' => [
                    'opportunities' => [
                        'label'       => 'Opportunities',
                        'url'         => Backend::url('empu/elementcrm/opportunities'),
                        'icon'        => 'icon-diamond',
                        'permissions' => ['empu.elementcrm.access_opportunities'],
                        'order'       => 200,
                    ],
                    // 'tasks' => [
                    //     'label'       => 'Tasks',
                    //     'url'         => Backend::url('empu/elementcrm/tasks'),
                    //     'icon'        => 'icon-list',
                    //     'permissions' => ['empu.elementcrm.access_tasks'],
                    //     'order'       => 200,
                    // ],
                    'companies' => [
                        'label'       => 'Companies',
                        'url'         => Backend::url('empu/elementcrm/companies'),
                        'icon'        => 'icon-building-o',
                        'permissions' => ['empu.elementcrm.access_companies'],
                        'order'       => 200,
                    ],
                ]
            ],
        ];
    }
}
